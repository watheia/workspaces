import React from "react"
import { render } from "@testing-library/react"
import { BasicAquaGradient } from "./aqua-gradient.composition"

it("should render with the correct text", () => {
  const { getByTestId } = render(<BasicAquaGradient />)
  const rendered = getByTestId("watheia.app/ui/design/aqua-gradient")
  expect(rendered).toBeTruthy()
})
