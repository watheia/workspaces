import React from "react"
import { AquaGradient } from "./aqua-gradient"

export const BasicAquaGradient = () => (
  <AquaGradient data-testid="watheia.app/ui/design/aqua-gradient" />
)
