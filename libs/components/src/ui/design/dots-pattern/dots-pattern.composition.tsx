import React from 'react';
import { DotsPattern } from './dots-pattern';

export const BasicDotsPattern = () => (
  <DotsPattern text="hello from DotsPattern" />
);
