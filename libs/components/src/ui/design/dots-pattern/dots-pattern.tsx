import React from 'react';

export type DotsPatternProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function DotsPattern({ text }: DotsPatternProps) {
  return (
    <div>
      {text}
    </div>
  );
}
