import React from 'react';
import { render } from '@testing-library/react';
import { BasicDotsPattern } from './dots-pattern.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicDotsPattern />);
  const rendered = getByText('hello from DotsPattern');
  expect(rendered).toBeTruthy();
});
