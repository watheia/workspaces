import React from 'react';
import { BrandIcon } from './brand-icon';

export const BasicBrandIcon = () => (
  <BrandIcon text="hello from BrandIcon" />
);
