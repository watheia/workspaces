import React from 'react';

export type BrandIconProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function BrandIcon({ text }: BrandIconProps) {
  return (
    <div>
      {text}
    </div>
  );
}
