import React from 'react';
import { render } from '@testing-library/react';
import { BasicBrandIcon } from './brand-icon.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicBrandIcon />);
  const rendered = getByText('hello from BrandIcon');
  expect(rendered).toBeTruthy();
});
