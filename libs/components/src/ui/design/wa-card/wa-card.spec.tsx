import React from "react"
import { render } from "@testing-library/react"
import { BasicWaCard } from "./wa-card.composition"

it("should render with the co", () => {
  const { getAllByTestId } = render(<BasicWaCard />)
  const rendered = getAllByTestId("watheia.app/design/wa-card")
  expect(rendered).toBeTruthy()
})
