import React from "react"
import { render } from "@testing-library/react"
import { BasicWaIcon } from "./wa-icon.composition"

it("should render with the correct text", () => {
  const { getByTestId } = render(<BasicWaIcon />)
  const rendered = getByTestId("watheia.app/design.wa-icon")
  expect(rendered).toBeTruthy()
})
