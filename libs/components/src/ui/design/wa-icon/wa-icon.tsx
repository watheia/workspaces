import React, { SVGProps } from "react"

export type WaIconProps = SVGProps<SVGSVGElement>

export function WaIcon(props: WaIconProps) {
  return (
    <svg
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 536 536"
      {...props}
    >
      <defs>
        <linearGradient
          id="prefix__a"
          x1={422.31}
          y1={371.44}
          x2={473.58}
          y2={280.96}
          gradientUnits="userSpaceOnUse"
        >
          <stop offset={0} stopColor="#00a995" />
          <stop offset={0.5} stopColor="#006eb9" />
          <stop offset={1} stopColor="#00aa4f" />
        </linearGradient>
        <linearGradient
          id="prefix__b"
          x1={56.54}
          y1={377.34}
          x2={149.75}
          y2={331.21}
          xlinkHref="#prefix__a"
        />
        <linearGradient
          id="prefix__c"
          x1={126.84}
          y1={404.12}
          x2={212.49}
          y2={383.87}
          xlinkHref="#prefix__a"
        />
        <linearGradient
          id="prefix__d"
          x1={337.6}
          y1={408.61}
          x2={405.59}
          y2={352.73}
          xlinkHref="#prefix__a"
        />
        <linearGradient
          id="prefix__e"
          x1={238.54}
          y1={391.15}
          x2={300.09}
          y2={408.72}
          xlinkHref="#prefix__a"
        />
      </defs>
      <path
        d="M180 268v-.47a88 88 0 01176 0v.47h160v-32.47a8 8 0 00-7-7.94l-63.33-7.91a182.19 182.19 0 00-18.2-43.94l39.2-50.4a8 8 0 00-.66-10.57l-45.28-45.23a8 8 0 00-10.57-.65l-50.4 39.2a182.36 182.36 0 00-43.9-18.16l-7.92-63.37a8 8 0 00-7.94-7h-64a8 8 0 00-7.94 7l-7.92 63.39a181.25 181.25 0 00-43.92 18.2l-50.4-39.2a8 8 0 00-10.57.65L70 114.8a8 8 0 00-.67 10.57l39.2 50.4a182.4 182.4 0 00-18.13 43.91L27 227.59a8 8 0 00-7 7.94V268z"
        fill="#1a1718"
      />
      <path
        d="M180 268v-.47a88 88 0 01176 0v.47h160v-32.47a8 8 0 00-7-7.94l-63.33-7.91a182.19 182.19 0 00-18.2-43.94l39.2-50.4a8 8 0 00-.66-10.57l-45.28-45.23a8 8 0 00-10.57-.65l-50.4 39.2a182.36 182.36 0 00-43.9-18.16l-7.92-63.37a8 8 0 00-7.94-7h-64a8 8 0 00-7.94 7l-7.92 63.39a181.25 181.25 0 00-43.92 18.2l-50.4-39.2a8 8 0 00-10.57.65L70 114.8a8 8 0 00-.67 10.57l39.2 50.4a182.4 182.4 0 00-18.13 43.91L27 227.59a8 8 0 00-7 7.94V268z"
        fill="#201c1d"
      />
      <path
        d="M460 356a31.62 31.62 0 00-16.16 4.53L404 320.69V284h-16v40a8 8 0 002.34 5.66l42.19 42.18A31.62 31.62 0 00428 388a32 32 0 1032-32z"
        fill="url(#prefix__a)"
      />
      <path
        d="M145.66 329.66A8 8 0 00148 324v-40h-16v36.69l-39.84 39.84A31.62 31.62 0 0076 356a32 32 0 1032 32 31.62 31.62 0 00-4.53-16.16z"
        fill="#04bfbf"
      />
      <path
        d="M145.66 329.66A8 8 0 00148 324v-40h-16v36.69l-39.84 39.84A31.62 31.62 0 0076 356a32 32 0 1032 32 31.62 31.62 0 00-4.53-16.16z"
        fill="url(#prefix__b)"
      />
      <path
        d="M196 352.69l-45.66 45.65A8 8 0 00148 404v33.13a32 32 0 1016 0v-29.82l45.66-45.65A8 8 0 00212 356v-72h-16z"
        fill="url(#prefix__c)"
      />
      <path
        d="M388 437.13V404a8 8 0 00-2.34-5.66L340 352.69V284h-16v72a8 8 0 002.34 5.66L372 407.31v29.82a32 32 0 1016 0z"
        fill="url(#prefix__d)"
      />
      <path d="M276 453.13V284h-16v169.13a32 32 0 1016 0z" fill="url(#prefix__e)" />
    </svg>
  )
}
