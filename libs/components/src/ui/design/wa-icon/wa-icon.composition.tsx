import React from "react"
import { WaIcon } from "./wa-icon"

export const BasicWaIcon = () => <WaIcon data-testid="watheia.app/design.wa-icon" />
