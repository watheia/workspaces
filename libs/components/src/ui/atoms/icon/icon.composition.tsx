import React from 'react';
import { Icon } from './icon';

export const BasicIcon = () => (
  <Icon text="hello from Icon" />
);
