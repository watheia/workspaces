import { HtmlHTMLAttributes } from "react"

interface ImageProps extends HtmlHTMLAttributes<HTMLImageElement> {
  /**
   * Lazy loading properties
   */
  lazyProps?: Record<string, any>
  /**
   * Should lazy load the image
   */
  lazy?: boolean
}
