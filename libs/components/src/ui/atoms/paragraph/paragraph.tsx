import React from "react"
import clsx from "clsx"
import { PossibleSizes } from "@watheia/app.theme.sizes"
import { textSize } from "@watheia/app.theme.sizes"
import { sansFont } from "@watheia/app.theme.fonts.clean"

export { PossibleSizes }

export type ParagraphProps = {
  /**
   * Font size (from a list of presets).
   */
  size: PossibleSizes
  /**
   * The underlying html element
   */
  element: "p" | "div" | "span"
} & React.HTMLAttributes<HTMLParagraphElement>

/**
 * Paragraph component prototype. Accepts all properties of a native Paragraph element,
 * @name paragraph
 * @example
 * <Paragraph size="sm">This is some text</Paragraph>
 */
export function Paragraph({
  className,
  size = PossibleSizes.md,
  element: Element = "p",
  ...rest
}: ParagraphProps) {
  return (
    <Element
      className={clsx(textSize[size], sansFont, className)}
      data-bit-id="watheia.app/ui/atoms/paragraph"
      {...rest}
    />
  )
}

Paragraph.defaultProps = {
  size: "md",
  element: "p"
}
