import React from "react"
import type { LinkProps } from "@watheia/app.routing.native-link"
import { useRouting } from "@watheia/app.routing.routing-provider"

export type { LinkProps }

export function Link(props: LinkProps) {
  const ActualLink = useRouting().Link
  return <ActualLink {...props} />
}
