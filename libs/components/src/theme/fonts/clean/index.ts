import styles from "./clean-font.module.scss"
export {
  StaticCleanFont,
  staticSansFontClass,
  staticSerifFontClass,
  staticCleanFontUrl
} from "./clean-font-link"

const { sansFont, serifFont } = styles
export { sansFont, serifFont }
