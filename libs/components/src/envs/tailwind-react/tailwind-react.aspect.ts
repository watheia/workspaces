import { Aspect } from '@teambit/harmony';

export const TailwindReactAspect = Aspect.create({
  id: 'watheia.app/envs/tailwind-react',
  defaultConfig: {},
});
