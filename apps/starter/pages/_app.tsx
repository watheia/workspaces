import Head from "next/head"
import "@watheia/app.theme.tailwind-styles/dist/styles.css"
import "./styles.css"

function WaApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Welcome to starter!</title>
      </Head>
      <div className="app">
        <header className="flex">
          {/* eslint-disable-next-line @next/next/no-img-element */}
          <img src="/nx-logo-white.svg" alt="Nx logo" width="75" height="50" />
          <h1>Welcome to starter!</h1>
        </header>
        <main>
          <Component {...pageProps} />
        </main>
      </div>
    </>
  )
}

export default WaApp
